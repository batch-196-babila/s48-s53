import React from 'react';

// Creates a Context Object
// Data type of an object that can be used to store information and can be shared to other components within the app
const UserContext = React.createContext();

// Provider component allows other components to consume/use the context object and supply the necessary info needed
export const UserProvider = UserContext.Provider;

export default UserContext;

// User context value when we set it up at the App.js

// UserContext = {
//     user: '',
//     setUser: function(),
//     unsetUser: function
// }