import { useState } from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';

// destructure at the parameter
export default function CourseCard({courseProp}) { 
    //console.log(props); // result: laravel (courseData[]0)

    // object destructuring
    const {name, description, price, _id} = courseProp; 
    
    // react hooks - useState() -> store its state
    // Syntax: const [getter, setter] = useState(initialGetterValue);
    // const [count, setCount] = useState(0);
    // const [seatCount, seatSetCount] = useState(10);

    // function enroll() {
    //     setCount(count + 1)
    //     console.log(`Enrollees: ${count}`);
    // }

    // function enroll() {
    //     setCount(count + 1);
    //     seatSetCount(seatCount - 1)
    //     console.log(`Enrollees: ${count}`);
    //     if (count === 10 && seatCount === 0) {
    //         alert("No more seats available. Try again");
    //         setCount(10);
    //         seatSetCount(0);
    //     }
    // }

    return (
        <Card className="mt-3 mb-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Course Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            </Card.Body>
        </Card>
    )
};