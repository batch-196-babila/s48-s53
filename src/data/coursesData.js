const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet nostrum mollitia explicabo sunt quasi illum, exercitationem ex voluptate unde in officiis! Incidunt dolores, veniam accusantium molestiae optio ut dolore possimus.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python-Django",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet nostrum mollitia explicabo sunt quasi illum, exercitationem ex voluptate unde in officiis! Incidunt dolores, veniam accusantium molestiae optio ut dolore possimus.",
        price: 55000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet nostrum mollitia explicabo sunt quasi illum, exercitationem ex voluptate unde in officiis! Incidunt dolores, veniam accusantium molestiae optio ut dolore possimus.",
        price: 65000,
        onOffer: true
    }
]

export default coursesData;