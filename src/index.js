import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import AppNavbar from './components/AppNavbarr';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* <AppNavbar /> */}
    <App />
  </React.StrictMode>
);

// const name = "Scarlet Spidey";
// const element = <h1>Hello, {name}</h1> // jsx - js extensible markup lang

// const user = {
//   firstName: 'Levi',
//   lastName: 'Ackerman'
// };

// const formatName = () => {
//   // return user.firstName + " " + user.lastName;
//   return  `${user.firstName} ${user.lastName}`;
// }

// const fullName = <h1>Hello, {formatName(user)}</h1>


// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullName);

