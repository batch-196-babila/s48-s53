// import {Row, Col, Button} from 'react-bootstrap';
// import { Link } from "react-router-dom";
import Banner from "../components/Banner";

export default function Error() {
    const data = {
        title: "404 - Not Found",
        content: "Enroll the courses that we offer",
        destination: "/",
        label: "Back home"
    }

    return (
        // <Row>
        //     <Col className="p-5">
        //         <h1>404 - Not Found</h1>
        //         <p>Enroll the courses that we offer</p>
        //         <Button variant="primary" as={Link} to="/">Back Home</Button>
        //     </Col>
        // </Row>

        <Banner data={data} />
    )
};