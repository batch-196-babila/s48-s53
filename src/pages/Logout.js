import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
    const {unsetUser, setUser} = useContext(UserContext);

    // localStorage.clear();
    unsetUser();


    // Placing the set user function inside of a useEffect is necessary because of updates within ReactJS that a state of another component cannot be updated while trying to render a different component

    // By adding useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user
    useEffect(() => {
        // set the user state back to it's origin state
        setUser({id: null})
    })

    return (
        <Navigate to ="/login" />
    )
};