import { useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

    const {user} = useContext(UserContext);
    const history = useNavigate();

    // state hooks to store the values of input data
    const [firstName , setFirstName] = useState('');
    const [lastName , setLastName] = useState('');
    const [mobileNo , setMobileNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // const [password2, setPassword2] = useState('');
    // state to determin whether submit is enabled or not
    const [isActive, setIsActive] = useState(false);

    // console.log(email);

    function registerUser(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/checkEmailExists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data); // false

            if (data) {
                Swal.fire({
                    title: "Duplicate email found",
                    icon: "info",
                    text: "The email that you're trying to register already exists."
                })
            }
            else {
                fetch('http://localhost:4000/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,                       
                        mobileNo: mobileNo,
                        email: email,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if (data.email) {
                        Swal.fire({
                            title: "Registration sucessful",
                            icon: "success",
                            text: "Thank you for registering."
                        })
                        history("/login");
                    }
                    else {
                        Swal.fire({
                            title: "Registration failed",
                            icon: "error",
                            text: "Something went wrong, try again."
                        })
                    }
                })
            }
        })

        // clear input field
        setEmail('');
        setPassword('');
        setFirstName('');
        setLastName('');
        setMobileNo('');
        
        // alert("Thank you for registering");
    }

    // Syntax: useEffect(() => {}, []);

    useEffect(() => {
        if (email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11) {
            setIsActive(true);
        } 
        else {
            setIsActive(false);
        }
    }, [email, firstName, lastName, mobileNo, password]); 
    // use effect will only work if there changes on the var insde []

    return (
        (user.id !== null) ?
            <Navigate to="/courses" />
        :
        <>
        <h1>Register</h1>
        <Form onSubmit={e => registerUser(e)}>
        <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter your firstname here" 
                    required 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter your firstname here" 
                    required 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter your 11-digit mobile number" 
                    required 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="userEmail">
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter your email here" 
                    required 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Enter your password here" 
                    required
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
            </Form.Group>
        { isActive ?
            <Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register</Button>
            :
            <Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Register</Button>
        }
        </Form>
        </>
    )
}